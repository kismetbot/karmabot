#!/usr/bin/env bash

set -euxo pipefail

if [[ "x${TRAVIS_TAG:-x}" == "xx" ]]; then
    exit 0
fi

echo "$DOCKER_PASSWORD" | docker login --username "$DOCKER_USERNAME" --password-stdin
docker build -f Dockerfile -t "$DOCKER_USERNAME/karmabot:$TRAVIS_TAG" .
docker tag "$DOCKER_USERNAME/karmabot:$TRAVIS_TAG" "$DOCKER_USERNAME/karmabot:latest"
docker push "$DOCKER_USERNAME/karmabot:latest"
docker push "$DOCKER_USERNAME/karmabot:$TRAVIS_TAG"
