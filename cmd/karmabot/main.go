package main

import (
	"flag"
	"strings"

	"github.com/hoshsadiq/karmabot"
	"github.com/hoshsadiq/karmabot/database"
	karmabotui "github.com/hoshsadiq/karmabot/ui"
	"github.com/hoshsadiq/karmabot/ui/blankui"
	"github.com/hoshsadiq/karmabot/ui/webui"

	"github.com/kamaln7/envy"
	"github.com/nlopes/slack"
	log "github.com/sirupsen/logrus"
)

// cli flags
var (
	token            = flag.String("token", "", "slack RTM token")
	dbpath           = flag.String("db", "./db.sqlite3", "path to sqlite database")
	maxpoints        = flag.Int("maxpoints", 6, "the maximum amount of points that users can give/take at once")
	leaderboardlimit = flag.Int("leaderboardlimit", 10, "the default amount of users to list in the leaderboard")
	debug            = flag.Bool("debug", false, "set debug mode")
	webuitotp        = flag.String("webui.totp", "", "totp key")
	webuipath        = flag.String("webui.path", "", "path to web UI files")
	webuilistenaddr  = flag.String("webui.listenaddr", "", "address to listen and serve the web ui on")
	webuiurl         = flag.String("webui.url", "", "url address for accessing the web ui")
	motivate         = flag.Bool("motivate", true, "toggle motivate.im support")
	blacklist        = make(karmabot.StringList, 0)
	reactji          = flag.Bool("reactji", true, "use reactji as karma operations")
	upvotereactji    = make(karmabot.StringList, 0)
	downvotereactji  = make(karmabot.StringList, 0)
	aliases          = make(karmabot.StringList, 0)
	selfkarma        = flag.Bool("selfkarma", false, "allow users to add/remove karma to themselves")
)

func main() {
	// logging

	ll := log.WithField("version", karmabot.Version).Logger

	// cli flags

	flag.Var(&blacklist, "blacklist", "blacklist users from having karma operations applied on them")
	flag.Var(&aliases, "alias", "alias different users to one user")
	flag.Var(&upvotereactji, "reactji.upvote", "a list of reactjis to use for upvotes")
	flag.Var(&downvotereactji, "reactji.downvote", "a list of reactjis to use for downvotes")

	envy.Parse("KB")
	flag.Parse()

	// startup

	ll.Info("starting karmabot")

	// reactjis

	// reactji defaults
	if len(upvotereactji) == 0 {
		_ = upvotereactji.Set("+1")
		_ = upvotereactji.Set("thumbsup")
		_ = upvotereactji.Set("thumbsup_all")
	}
	if len(downvotereactji) == 0 {
		_ = downvotereactji.Set("-1")
		_ = downvotereactji.Set("thumbsdown")
	}
	reactjiConfig := &karmabot.ReactjiConfig{
		Enabled:  *reactji,
		Upvote:   upvotereactji,
		Downvote: downvotereactji,
	}

	// format aliases
	aliasMap := make(karmabot.UserAliases, 0)
	for k := range aliases {
		users := strings.Split(k, "++")
		if len(users) <= 1 {
			ll.Fatal("invalid alias format. see documentation")
		}

		user := users[0]
		for _, alias := range users[1:] {
			aliasMap[alias] = user
		}
	}

	// database

	db, err := database.New(&database.Config{
		Path: *dbpath,
	})

	if err != nil {
		ll.WithField("path", *dbpath).WithError(err).Fatal("could not open sqlite db")
	}

	// slack

	if *token == "" {
		ll.Fatal("please pass the slack RTM token (see `karmabot -h` for help)")
	}

	sc := slack.New(*token,
		slack.OptionDebug(*debug),
		slack.OptionLog(&LogrusLogProvider{Log: ll}),
	).NewRTM()
	go sc.ManageConnection()

	// karmabot

	var ui karmabotui.Provider
	if *webuipath != "" && *webuilistenaddr != "" {
		ui, err = webui.New(&webui.Config{
			ListenAddr:       *webuilistenaddr,
			URL:              *webuiurl,
			FilesPath:        *webuipath,
			TOTP:             *webuitotp,
			LeaderboardLimit: *leaderboardlimit,
			Log:              ll.WithField("provider", "webui").Logger,
			Debug:            *debug,
			DB:               db,
		})

		if err != nil {
			ll.WithError(err).Fatal("could not initialize web ui")
		}
	} else {
		ui = blankui.New()
	}
	go ui.Listen()

	bot := karmabot.New(&karmabot.Config{
		Slack:            &karmabot.SlackChatService{RTM: *sc},
		UI:               ui,
		Debug:            *debug,
		MaxPoints:        *maxpoints,
		LeaderboardLimit: *leaderboardlimit,
		Log:              ll,
		DB:               db,
		UserBlacklist:    blacklist,
		Reactji:          reactjiConfig,
		Motivate:         *motivate,
		Aliases:          aliasMap,
		SelfKarma:        *selfkarma,
	})

	bot.Listen()
}

type LogrusLogProvider struct {
	Log *log.Logger
}

func (l *LogrusLogProvider) Output(calldepth int, message string) error {
	l.Log.WithField("calldepth", calldepth).Info(message)
	return nil
}
