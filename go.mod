module github.com/hoshsadiq/karmabot

require (
	github.com/aybabtme/log v0.0.0-20170418131122-ba6ae9871c28
	github.com/boombuler/barcode v1.0.0 // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/go-kit/kit v0.7.0 // indirect
	github.com/go-logfmt/logfmt v0.3.0 // indirect
	github.com/go-stack/stack v1.7.0 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/gorilla/websocket v1.2.0 // indirect
	github.com/kamaln7/envy v1.1.0
	github.com/kr/logfmt v0.0.0-20140226030751-b84e30acd515 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lusis/go-slackbot v0.0.0-20180109053408-401027ccfef5 // indirect
	github.com/lusis/slack-test v0.0.0-20180109053238-3c758769bfa6 // indirect
	github.com/mattn/go-sqlite3 v1.9.0
	github.com/nlopes/slack v0.5.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pquerna/otp v1.0.0
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.3.0
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/urfave/cli v1.20.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
