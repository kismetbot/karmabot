FROM alpine:latest
RUN apk add --no-cache ca-certificates
COPY karmabot_linux_amd64 /
COPY www /www
EXPOSE 4000
ENV KB_WEBUI_LISTENADDR 0.0.0.0:4000
ENV KB_WEBUI_PATH /www
ENTRYPOINT ["/karmabot_linux_amd64"]
